import React from "react";
import './login.css';
import { BsFillEnvelopeFill } from "react-icons/bs";
import { BsFillLockFill } from "react-icons/bs";

export default class Login extends React.Component {
    render(){
        return(
            <div>
                <div class="card float-end m-4 rounded-pill border-0">
                <div class="card-header">
                    <h4>User Login</h4>
                </div>
                <div class="card-body">
                    <form className="needs-validation" novalidate>
                        <div class="mb-4 position-relative has-validation">
                            <BsFillEnvelopeFill className="emailIcon position-absolute"></BsFillEnvelopeFill>
                            <input type="email" class="form-control shadow-none border-0 border-bottom px-4 bg-transparent" id="exampleInputEmail1" placeholder="Email ID" required/>
                            <div class="invalid-feedback">
                                Please Enter Username.
                            </div>
                        </div>
                        <div class="mb-4 position-relative">
                            <BsFillLockFill className="emailIcon position-absolute"/>
                            <input type="password" class="form-control shadow-none border-0 border-bottom px-4 bg-transparent" placeholder="Password" id="exampleInputPassword1" required/>
                        </div>  
                        <div class="mb-3 form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1"/>
                            <label class="form-check-label float-start" style={{color:"#cccccc"}} for="exampleCheck1">Remember me</label>
                        </div>                  
                        <button type="submit" class="btn btn-secondary">Login</button>
                    </form>
                    <p style={{color:"#cccccc",fontSize:'13px'}} className="mt-2">Don't have account?, <a style={{color:"#CCCD21"}}>Signup</a> .</p>
                </div>
                </div>   
            </div>                            
        )
    }
}