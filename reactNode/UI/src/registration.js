import * as React from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import './registration.css'

export default function Registration() {
    const [age, setAge] = React.useState('');

  const handleChange = (event) => {
    setAge(event.target.value);
  };
  return (
    <Box
      sx={{
        display: 'flex',       
        '& > :not(style)': {
          m: 1,
          width: 500,
          height: 500          
        },
      }}
    >
      <Paper variant="outlined" sx={{p:1,backgroundColor:'#7f8180'}}>
      <TextField sx={{m:1,backgroundColor:'#fff'}} id="filled-basic" label="Firstname" variant="filled" />
      <TextField sx={{m:1,backgroundColor:'#fff'}}id="filled-basic" label="Lastname" variant="filled" />
      <FormControl variant="standard" sx={{ m: 1,p:1, minWidth: 217,backgroundColor:'#fff',opacity:'0.9' }}>
        <InputLabel id="demo-simple-select-standard-label">Gender</InputLabel>
        <Select
          labelId="demo-simple-select-standard-label"
          id="demo-simple-select-standard"
          value={age}
          onChange={handleChange}
          label="Age"
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={10}>Ten</MenuItem>
          <MenuItem value={20}>Twenty</MenuItem>
          <MenuItem value={30}>Thirty</MenuItem>
        </Select>
      </FormControl>

      <TextField sx={{m:1,backgroundColor:'#fff'}} id="filled-basic" label="Username" variant="filled" />
      <TextField sx={{m:1,backgroundColor:'#fff'}} id="filled-basic" label="Password" variant="filled" />
      <TextField sx={{m:1,backgroundColor:'#fff'}} id="filled-basic" label="Confirm Password" variant="filled" />
      <TextField sx={{m:1,backgroundColor:'#fff'}} id="filled-basic" label="Email" variant="filled" />
      <TextField sx={{m:1,backgroundColor:'#fff'}} id="filled-basic" type="number" label="Phone" variant="filled" />
      </Paper>
    </Box>
  );
}