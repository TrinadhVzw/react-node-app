import axios from "axios";
import React, { useEffect, useState } from "react";

// export default class Todos extends React.Component{
//     state = {
//         todosList : []
//     }
//     componentDidMount(){
//         axios.get('https://jsonplaceholder.typicode.com/todos')
//         .then(res=>{ 
//             let todosList = res.data;           
//             this.setState({todosList:res.data});
//             console.log("data",this.state.todosList);
//         })                
//     }    
//     render(){
//         return(
//         <div>
//             {this.state.todosList.map(todo=>{
//                 <p>{todo.title}</p>
//             })}
//         </div>
//         )
//     }
// }

 const Todos = ()=>{
    const[todoList,setTodoList] = useState([]);
    useEffect(()=>{
                 axios.get('https://jsonplaceholder.typicode.com/todos')
                 .then(res=>{
                    setTodoList(res.data);
                    console.log('data',todoList)
                 })
    },[])
    return(
        <div></div>
    )
}
export default Todos