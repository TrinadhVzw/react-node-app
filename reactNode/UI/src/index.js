import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import PrimarySearchAppBar from './home';
import Test from './test';
import Registration from './registration';
// import StickyHeadTable from './todos';
import Todos from './todos';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import TemporaryDrawer from './navMenu';
import { Route, Link, BrowserRouter as Router, Switch } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
const routes = (
  <Router>
    <Route exact path="/" component={App} />    
    <Route path="/home" component={PrimarySearchAppBar} />    
    <Route path="/nav" component={TemporaryDrawer} />    
    <Route path="/test" component={Test} />    
    <Route path="/todos" component={Todos} />    
    <Route path="/register" component={Registration} />    
  </Router>
)

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    routes
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
